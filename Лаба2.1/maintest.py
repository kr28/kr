from main import sort_1, sort_2, sort_3, sort_4
testList_1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
testList_2 = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
testList_3 = [9, 3, 5, 8, 2, 10, 1, 6, 4, 7]
correctResult = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
def test_sort_1():
    assert sort_1(testList_1) == correctResult
    assert sort_1(testList_2) == correctResult
    assert sort_1(testList_3) == correctResult
def test_sort_2():
    assert sort_2(testList_1) == correctResult
    assert sort_2(testList_2) == correctResult
    assert sort_2(testList_3) == correctResult
def test_sort_3():
    assert sort_3(testList_1) == correctResult
    assert sort_3(testList_2) == correctResult
    assert sort_3(testList_3) == correctResult
def test_sort_4():
    assert sort_4(testList_1) == correctResult
    assert sort_4(testList_2) == correctResult
    assert sort_4(testList_3) == correctResult
