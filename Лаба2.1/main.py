from random import randint
import time
from memory_profiler import profile
import math
N = 10
a = []
for i in range(N):
    a.append(randint(1, 99))
print(a, 'Рандомный список', '\n')
mem_profile_file = open("mem_profile.txt", "w+")


############################### Пузырьковая сортировка###############################
@profile(stream=mem_profile_file)
def sort_1(a):
    for i in range(N - 1):
        for j in range(N - i - 1):
            if a[j] > a[j + 1]:
                a[j], a[j + 1] = a[j + 1], a[j]
    return a
timeStart_1 = time.perf_counter()
sort_1(a)
timeEnd_1 = time.perf_counter() - timeStart_1
print(a, 'Пузырьковая сортировка')
print(f"Пузырьковая сортировка время : {timeEnd_1}", '\n')

############################### Сортировка вставками ###############################
@profile(stream=mem_profile_file)
def sort_2(a):
    for i in range(1, len(a)):
        key = a[i]
        j = i - 1
        while j >= 0 and key < a[j]:
            a[j + 1] = a[j]
            j -= 1
        a[j + 1] = key
    return a
timeStart_2 = time.perf_counter()
sort_2(a)
timeEnd_2 = time.perf_counter() - timeStart_2
print(a, 'Сортировка вставками')
print(f"Сортировка вставками время : {timeEnd_2}", '\n')


############################### Cортировка Шелла  ###############################
@profile(stream=mem_profile_file)
def sort_3(a):
    inc = len(a) // 2
    while inc:
        for i, el in enumerate(a):
            while i >= inc and a[i - inc] > el:
                a[i] = a[i - inc]
                i -= inc
            a[i] = el
        inc = 1 if inc == 2 else int(inc * 5.0 / 11)
    return a

timeStart_3 = time.perf_counter()
sort_3(a)
timeEnd_3 = time.perf_counter() - timeStart_3
print(a, 'Cортировка Шелла')
print(f"Сортировка Шелла время : {timeEnd_3}", '\n')


############################### Быстрая сортировка ###############################

@profile(stream=mem_profile_file)
def sort_4(a):
    if len(a) < 2:
        return a
    else:
        pivot = a[0]
        rest = a[1:]
        low = [each for each in rest if each < pivot]
        high = [each for each in rest if each >= pivot]
        return sort_4(low) + [pivot] + sort_4(high)

timeStart_4 = time.perf_counter()
sort_4(a)
timeEnd_4 = time.perf_counter() - timeStart_4
print(a, 'Быстрая сортировка')
print(f"Быстрая сортировка время : {timeEnd_4}")





