# Алгоритм  Краммера
systEquat = [[12, 21, 1, 51], [22, 12, 20, 0], [40, 0, 122, 0]]
def det(mat):
    res = 1
    matLength = len(mat)
    for i in range(matLength):
        res *= mat[i][i]
        for j in range(i + 1, matLength):
            b = mat[j][i] / mat[i][i]
            mat[j] = [mat[j][k] - b * mat[i][k] for k in range(matLength)]
    return int(res)
mainDelta = []
tempMatx = []
delta = []
matxX = []
tempMatx2 = []
count = 0
for i in range(0, len(systEquat)):
    for j in range(0, len(systEquat[i]) - 1):
        tempMatx.append(systEquat[i][j])
    mainDelta.append(tempMatx)
    tempMatx = []

for z in range(0, len(systEquat)):
    for i in range(0, len(systEquat)):
        matxX.append(systEquat[i][len(systEquat)])
        for j in range(0, len(systEquat[i])):
            if j == count:
                tempMatx2.append(matxX[i])
                continue
            tempMatx2.append(systEquat[i][j])
        delta.append(tempMatx2)
        tempMatx2 = []
    count += 1
    print(det(delta) / det(mainDelta))  
    delta = []

